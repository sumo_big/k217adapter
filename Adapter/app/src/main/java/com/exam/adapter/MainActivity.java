package com.exam.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.ListView;

import com.exam.adapter.fragments.HomeFragment;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addFragment("home", false);

    }

    public void addFragment(String key, boolean addToStack){

        Fragment fragment = new HomeFragment();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ts = manager.beginTransaction();

        ts.replace(R.id.containt, fragment);
        if(addToStack){
            ts.addToBackStack(key);
        }

        ts.commit();
    }

}
