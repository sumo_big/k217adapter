package com.exam.adapter.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.exam.adapter.HSAdapter;
import com.exam.adapter.MainActivity;
import com.exam.adapter.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC14-02 on 11/13/2015.
 */
public class HomeFragment extends Fragment{

    private HSAdapter adapter;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null, false);

        /*Button btn = (Button) view.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity main = (MainActivity) getActivity();
                main.addFragment("detail", true);
            }
        });
*/
        List<String>hs = new ArrayList<>();
        for(int i=0; i<50; i++){
            hs.add("Hoc sinh " + i );
        }

        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new HSAdapter(getActivity());
        adapter.setData(hs);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("click - " + position);
            }
        });

        return view;
    }



}
