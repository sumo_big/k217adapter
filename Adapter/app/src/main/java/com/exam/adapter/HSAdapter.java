package com.exam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by PC14-02 on 11/13/2015.
 */
public class HSAdapter extends BaseAdapter{

    private List<String> data;
    Context context;

    public HSAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.layout_item_hocsinh, null, false);
        TextView tenTV = (TextView) v.findViewById(R.id.tenhs_textView);

        tenTV.setText( (String)getItem(position) );

        return v;
    }
}
